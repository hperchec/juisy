## 1.4.0 (2024-11-27)

* feat: add type option to squeeze command ([2dc93d0](https://gitlab.com/hperchec/juisy/commit/2dc93d0))
* fix: bad config file path for "module" template ([9836c68](https://gitlab.com/hperchec/juisy/commit/9836c68))



## 1.3.0 (2024-11-24)

* feat: split template to commonjs and module types ([d16a125](https://gitlab.com/hperchec/juisy/commit/d16a125))
* feat: support for projects that have "type": "module" in package.json ([99ae502](https://gitlab.com/hperchec/juisy/commit/99ae502))
* feat(squeeze): add prompt for project type (commonjs/module) ([47a53f2](https://gitlab.com/hperchec/juisy/commit/47a53f2))
* fix: remove bugs and repository properties from example package.json ([d0ba6d7](https://gitlab.com/hperchec/juisy/commit/d0ba6d7))
* docs: update docs ([c0c46a7](https://gitlab.com/hperchec/juisy/commit/c0c46a7))



## <small>1.2.4 (2024-06-25)</small>

* build: update dependencies ([8f051e3](https://gitlab.com/hperchec/juisy/commit/8f051e3))



## <small>1.2.3 (2024-06-25)</small>

* build: update eslint ([ae77341](https://gitlab.com/hperchec/juisy/commit/ae77341))
* build: update peer dependencies ([52988aa](https://gitlab.com/hperchec/juisy/commit/52988aa))



## <small>1.2.2 (2024-06-25)</small>

* build: update peer dependencies ([4ce7c5e](https://gitlab.com/hperchec/juisy/commit/4ce7c5e))



## <small>1.2.1 (2024-06-25)</small>

* fix: bad config path for docs generate:api command ([654326f](https://gitlab.com/hperchec/juisy/commit/654326f))
* build: update peer dependencies ([a1ae1fa](https://gitlab.com/hperchec/juisy/commit/a1ae1fa))



## 1.2.0 (2024-06-25)

* build: add eslint & commitlint ([b52acde](https://gitlab.com/hperchec/juisy/commit/b52acde))
* build: add example folder ([6122b59](https://gitlab.com/hperchec/juisy/commit/6122b59))
* build: add peerDependencies ([5b36384](https://gitlab.com/hperchec/juisy/commit/5b36384))
* build: configure commitlint ([0e03b2f](https://gitlab.com/hperchec/juisy/commit/0e03b2f))
* build: configure git hooks ([2e19803](https://gitlab.com/hperchec/juisy/commit/2e19803))
* build: disable npm install on example:test script ([8930b4b](https://gitlab.com/hperchec/juisy/commit/8930b4b))
* build(cli): add confirm util ([ef71dd6](https://gitlab.com/hperchec/juisy/commit/ef71dd6))
* build(cli): generate docs at release ([e1e58c2](https://gitlab.com/hperchec/juisy/commit/e1e58c2))
* build(cli): remove unused argument ([7a4dea6](https://gitlab.com/hperchec/juisy/commit/7a4dea6))
* build(cli): update release command commit message + remove auto push ([4d25cc1](https://gitlab.com/hperchec/juisy/commit/4d25cc1))
* fix: add missing env variable for private commands ([a353e77](https://gitlab.com/hperchec/juisy/commit/a353e77))
* fix: bad commit-msg command injection ([bc6fd56](https://gitlab.com/hperchec/juisy/commit/bc6fd56))
* fix: require missing deps in template ([901e50d](https://gitlab.com/hperchec/juisy/commit/901e50d))
* feat: add git-hooks commands to template ([ba0f538](https://gitlab.com/hperchec/juisy/commit/ba0f538))
* feat: upgrade @hperchec/readme-generator to v3 ([ec8bc3b](https://gitlab.com/hperchec/juisy/commit/ec8bc3b))
* refactor: add bugs informations in example package.json ([60ecb3e](https://gitlab.com/hperchec/juisy/commit/60ecb3e))
* refactor: add repository informations in example package.json ([1cd9360](https://gitlab.com/hperchec/juisy/commit/1cd9360))
* refactor: delete scripts/cli-doc.js ([8cf73f2](https://gitlab.com/hperchec/juisy/commit/8cf73f2))
* refactor: move specific api docs files to .docs/api ([621ce60](https://gitlab.com/hperchec/juisy/commit/621ce60))
* refactor: set private commands ([9f81937](https://gitlab.com/hperchec/juisy/commit/9f81937))
* refactor: v2 features ([5384b45](https://gitlab.com/hperchec/juisy/commit/5384b45))
* style: update & remove unnecessary comments ([d00bf21](https://gitlab.com/hperchec/juisy/commit/d00bf21))
* docs: docs configuration ([07b37ec](https://gitlab.com/hperchec/juisy/commit/07b37ec))
* docs: update readme ([984cd0c](https://gitlab.com/hperchec/juisy/commit/984cd0c))
* wip: prepare for v2 ([e327905](https://gitlab.com/hperchec/juisy/commit/e327905))
* wip: prepare v2 ([c2b0686](https://gitlab.com/hperchec/juisy/commit/c2b0686))
* wip: prepare v2 ([202a0a1](https://gitlab.com/hperchec/juisy/commit/202a0a1))
* test ([1727890](https://gitlab.com/hperchec/juisy/commit/1727890))
* test ([b9ec7fe](https://gitlab.com/hperchec/juisy/commit/b9ec7fe))
* Update .gitignore ([1b4570d](https://gitlab.com/hperchec/juisy/commit/1b4570d))
* Update README.md ([39f0531](https://gitlab.com/hperchec/juisy/commit/39f0531))
* Update README.md ([5de0e75](https://gitlab.com/hperchec/juisy/commit/5de0e75))
* wip ([8e39cb1](https://gitlab.com/hperchec/juisy/commit/8e39cb1))
* wip ([1b6b0b0](https://gitlab.com/hperchec/juisy/commit/1b6b0b0))



# [1.1.0](https://gitlab.com/hperchec/juisy/compare/v1.0.1...v1.1.0) (2023-04-25)


### Features

* add getFileUrls util (glob package) ([f4b3a9b](https://gitlab.com/hperchec/juisy/commit/f4b3a9be8c1d55d9d2fe5a2ea9c1f8fa7a0cd37b))



## [1.0.1](https://gitlab.com/hperchec/juisy/compare/v1.0.0...v1.0.1) (2022-10-06)



# [1.0.0](https://gitlab.com/hperchec/juisy/compare/v1.0.0-beta.7...v1.0.0) (2022-10-06)



# [1.0.0-beta.7](https://gitlab.com/hperchec/juisy/compare/v1.0.0-beta.6...v1.0.0-beta.7) (2022-10-06)



# 1.0.0-beta.6 (2022-10-06)



