<!--
  ⚠ IMPORTANT
  This file is generated with **@hperchec/readme-generator**. Don't edit it.
-->
# 🍊 juisy

<!-- markdownlint-disable line-length -->
![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

[![release](https://img.shields.io/gitlab/v/release/<%= projectPath %>?include_prereleases&logo=gitlab&sort=semver)](<%= projectUrl %>/-/releases)
[![pipeline-status](<%= projectUrl %>/badges/main/pipeline.svg)](<%= projectUrl %>/commits/main)
[![package](https://img.shields.io/npm/v/<%= packageName %>?logo=npm)](<%= packageUrl %>)
[![downloads](https://img.shields.io/npm/dw/<%= packageName %>?logo=npm)](<%= packageUrl %>)
[![issues](https://img.shields.io/gitlab/issues/open/<%= projectPath %>?gitlab_url=https%3A%2F%2Fgitlab.com)](<%= issuesUrl %>)
![license](https://img.shields.io/gitlab/license/<%= projectPath %>?gitlab_url=https%3A%2F%2Fgitlab.com)
<!-- markdownlint-enable line-length -->

**Table of contents**:

<!-- markdownlint-disable-next-line emphasis-style -->
[[_TOC_]]

## What is juisy?


- 🔧 [Embedded CLI tool](https://gitlab.com/hperchec/juisy/-/blob/main/documentation/api.md#juisycli--yargs) that uses yargs under the ground
- 📕 Generate your project documentation and README file with [@hperchec/readme-generator](https://www.npmjs.com/package/@hperchec/readme-generator)
- 🕵️‍♂️ Automatic linting with [lint-staged](https://www.npmjs.com/package/lint-staged) at pre-commit git hook
- 🦾 Respect [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) with [commitlint](https://commitlint.js.org/) at commit-msg git hook

## 🚀 Get started

Install package:

```sh
npm install <%= packageName %>
```

Squeeze!

```sh
npx juisy squeeze
```

You will get:

```sh
./
├─ bin/ # contains CLI
│  └─ cli/
│     ├─ cmds/ # commands dir
│     ├─ lib/ # libraries used by commands
│     └─ index.js # CLI entry file
├─ .docs/
│  └─ readme # contains configuration for @hperchec/readme-generator
└─ ...
```

...and some new scripts in your `package.json` file:

```js
"docs", // generate documentation
"docs:api", // only api
"docs:readme", // only README file
"release", // make a release
"changelog" // generate CHANGELOG file
"postinstall" // for git hook
```

Just use `--help` option to show help:

```sh
node ./bin/cli --help
```

## 🧱 Dependencies

It will install the following packages as peer dependencies:

<%= peerDependencies %>

## 📙 Documentation

### API

> See [API] documentation](https://gitlab.com/hperchec/juisy/-/blob/main/documentation/api.md)

### Utils

> See [utils documentation](https://gitlab.com/hperchec/juisy/-/blob/main/documentation/utils.md)

## ✍ Author

<%= author %>

## 📄 License

<%= license %>

----

Made with ❤ by <%= author %>
