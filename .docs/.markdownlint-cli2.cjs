const {
  getFileUrls,
  rootDir
} = require('../src').utils

// markdownlint-github
// See also: https://www.npmjs.com/package/@github/markdownlint-github
const options = require('@github/markdownlint-github').init()

/**
 * Export markdownlint config
 */
module.exports = {
  showFound: true,
  /**
   * At markdownlint-cli2 v0.10.0, the globs passed as CLI arguments are broken
   * So, we use node-glob locally to match file path. Ignored files are passed in "ignore" option.
   * See also https://github.com/isaacs/node-glob#options
   */
  globs: [
    ...getFileUrls(
      // All files (recursive) with .md, .markdown or .mdx extension
      '**/*.{md,markdown,mdx}',
      {
        cwd: rootDir, // relative to project root folder
        dot: true, // includes '.' (dot) files
        posix: true, // resolve as posix
        ignore: [
          // files to ignore
          'node_modules/**',
          '**/CHANGELOG.md'
        ]
      }
    )
  ],
  config: {
    ...options,
    // MD004 - Unordered list style
    'ul-style': {
      'style': 'consistent'
    },
    // MD036 - Emphasis used instead of a heading
    'no-emphasis-as-heading': false // disable for dmd template
  },
  customRules: [
    '@github/markdownlint-github'
  ],
  outputFormatters: [
    [
      // Use prettier output formatter
      'markdownlint-cli2-formatter-pretty',
      {
        appendLink: true // ensures the error message includes a link to the rule documentation
      }
    ]
  ]
}