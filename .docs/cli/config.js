const path = require('path')

/**
 * @hperchec/readme-generator configuration file
 * See also: https://github.com/hperchec/readme-generator
 */
module.exports = {
  /**
   * Output file name: 'README.md' by default
   */
  fileName: 'cli.md',
  /**
   * Change destination folder
   */
  destFolder: path.resolve(__dirname, '../../documentation'),
  /**
   * Template entry file path
   */
  templatePath: path.resolve(__dirname, './template.md'),
  /**
   * EJS data file
   */
  ejsDataPath: path.resolve(__dirname, './data.js')
}
