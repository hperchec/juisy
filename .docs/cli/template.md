# CLI usage

**Table of contents**

<!-- toc -->

**Usage**

```console
<%- $filters.stripAnsi(rootDoclet.rawUsage) %>
```

----

<% for (const child in rootDoclet.children) { %>
<%-
  await include('partials/child-command.md', {
    cmdDoclet: rootDoclet.children[child],
    hLevel: 2,
    recursive: true
  })
%>
<% } %>
