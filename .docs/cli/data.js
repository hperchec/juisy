/**
 * @hperchec/readme-generator Template EJS data file
 */

const stripAnsi = require('strip-ansi')
const { cliTools } = require('../../src')
const cliFactory = require('../../bin/cli/cli.js')

// async module => returns promise
module.exports = (async () => ({
  // Pass root doclet
  rootDoclet: await cliTools.extractUsage(cliFactory, true), // recursively extract usage
  // Add filters
  $filters: {
    stripAnsi
  }
}))()
