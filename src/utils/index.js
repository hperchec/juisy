const chalk = require('chalk')
const deepmerge = require('deepmerge')
const path = require('path')
const { readdir: readdirAsync } = require('fs').promises
const glob = require('glob')
const execa = require('execa')
const prompts = require('prompts')
const stripAnsi = require('strip-ansi')
const indent = require('indent-string')

/**
 * @alias utils.rootPath
 * @type {string}
 * @description
 * Get root directory path
 * @example
 * const { rootDir } = require('@hperchec/juisy').utils
 * console.log(rootDir) // => 'path/to/your/root/dir'
 */
// const rootDir = process.env.NODE_ENV === 'development'
//   ? process.cwd()
//   : path.resolve(__dirname, '../../../../../')
const rootDir = process.cwd()

/**
 * @alias utils.packageJson
 * @type {object}
 * @description
 * Get package.json content as object
 * @example
 * const { packageJson } = require('@hperchec/juisy').utils
 * console.log(packageJson) // => { ... }
 */
const packageJson = require(path.resolve(rootDir, './package.json'))

/**
 * @alias utils.$style
 * @type {object}
 * @description
 * Use `chalk` package to style output in console/stdout
 * @example
 * const { $style } = require('@hperchec/juisy').utils
 * console.log($style.green('Green text!')) // => '[32mGreen text![0m'
 */
const $style = chalk

/**
 * loading object
 * @ignore
 */
const loading = {
  // Duration of each character displaying (in ms)
  velocity: 250,
  // Reference to call setInterval and clearInterval
  intervalRef: null,
  // Display loading to STDOUT
  display: function (message) {
    const P = [ '\\', '|', '/', '-' ]
    const S = [ '   ', '.  ', '.. ', '...' ]
    let x = 0
    let y = 0
    const velocity = this.velocity
    this.intervalRef = setInterval(function () {
      process.stdout.write('\r' + P[x++] + ' ' + message + ' : ' + S[y++])
      x &= 3
      y &= 3
    }, velocity)
  },
  stop: function () {
    clearInterval(this.intervalRef)
    // Wrap in try/catch block because process.stdout.clearLine (or cursorTo) is non-TTY
    // This causes an error if command is executed by php or npm (for example)
    try {
      process.stdout.clearLine()
      process.stdout.cursorTo(0)
    } catch (err) {
      console.log('Warn: process.stdout.clearLine (or cursorTo) function is non-TTY. Skipped...')
    }
  }
}

/**
 * To automatically increment step index
 * @ignore
 */
const stepsCache = {
  index: 0 // 0 by default
}

/**
 * @alias utils.merge
 * @param {...any} args Same as deepmerge arguments
 * @returns {object} Returns merged object, see deepmerge documentation
 * @description
 * Deep merge two objects (see: https://www.npmjs.com/package/deepmerge)
 * @example
 * const { merge } = require('@hperchec/juisy').utils
 * merge({ a: 'foo', b: 'bar' }, { a: 'fuu', c: 'bax' }) // => { a: 'fuu', b: 'bar', c: 'bax' }
 */
function merge (...args) {
  return deepmerge(...args)
}

/**
 * @alias utils.getFileUrls
 * @param {string} pattern - Same as node-glob pattern argument
 * @param {object} options - Same as node-glob pattern argument
 * @returns {string[]} Returns an array of file path
 * @description
 * Get file urls that match glob (see: https://github.com/isaacs/node-glob)
 * @example
 * const { getFileUrls } = require('@hperchec/juisy').utils
 * getFileUrls('path/to/*.js') // => [ 'path/to/file1.js', 'path/to/file2.js', ... ]
 */
function getFileUrls (pattern, options) {
  const files = glob.sync(pattern, options)
  return files
}

/**
 * @alias utils.getFiles
 * @param {string} dir - The directory path
 * @returns {string[]} - Returns an array of file path
 * @yields {string[]} The next found files.
 * @description
 * Get all files in a directory (recursively)
 * @example
 * const { getFiles } = require('@hperchec/juisy').utils
 * getFiles(path) // => [ 'path/to/file1', 'path/to/file2', ... ]
 */
async function * getFiles (dir) {
  const dirents = await readdirAsync(dir, { withFileTypes: true })
  for (const dirent of dirents) {
    const res = path.resolve(dir, dirent.name)
    if (dirent.isDirectory()) {
      yield * getFiles(res)
    } else {
      yield res
    }
  }
}

/**
 * @alias utils.wait
 * @param {string} message - Message to display
 * @param {Function} fct - The function to execute
 * @returns {Promise<void>}
 * @description
 * Wait function: display loading during 'fct' execution
 * @example
 * const { wait } = require('@hperchec/juisy').utils
 * await wait('Waiting', async () => {
 *   // Do async logic
 * })
 */
async function wait (message, fct) {
  loading.display(message)
  await fct()
  loading.stop()
}

/**
 * @alias utils.run
 * @param {string} bin - Command
 * @param {string[]} args - Same as execa second arg
 * @param {object} [opts] - Options
 * @returns {Promise<object>} The `execa` Promise
 * @description
 * Run command (child_process). See also `execa` package documentation
 * @example
 * const { run } = require('@hperchec/juisy').utils
 * await run('npm', [ 'run', 'test' ], { stdio: 'inherit' })
 */
function run (bin, args, opts = {}) {
  return execa(bin, args, { stdio: 'inherit', cwd: rootDir, ...opts })
}

/**
 * @alias utils.log
 * @param {string} msg - The message
 * @param {object} [options] - (Optional) Options object
 * @param {number} [options.indent] - How many repeat indent (see `indent-string` package)
 * @param {string} [options.indentChar] - Indent character(s). If `options.indent` not provided, will be repeated only 1 time
 * @returns {void}
 * @description
 * Log message in console
 * @example
 * const { log } = require('@hperchec/juisy').utils
 * log() // => blank line
 * log('Hello world! =)') // => 'Hello world! =)'
 * log('To do:', { indent: 2 }) // => '  To do:'
 * log('laundry\nshopping', { indentChar: '- ' }) // => '- laundry\n- shopping'
 * log('foo\nbar', { indent: 2, indentChar: '❤' }) // => '❤❤foo\n❤❤bar'
 */
function log (msg, options = {}) {
  if (options.indentChar) {
    if (options.indent === undefined) {
      options.indent = 1
    }
  } else {
    options.indentChar = ' '
    options.indent = options.indent || 0
  }
  // If msg is defined
  if (msg) {
    msg = options.indent
      ? indent(msg, options.indent, { indent: options.indentChar })
      : msg
    console.log(msg)
  } else {
    // Else, just log blank line
    console.log()
  }
}

/**
 * @alias utils.step
 * @param {string} msg - The message to display
 * @param {object} [options] - Options object
 * @param {?number|string} [options.index] - Custom index. If `null`, no index prepended to string
 * @returns {void}
 * @description
 * Log step title
 * @example
 * const { step } = require('@hperchec/juisy').utils
 * step('Important step') // => '● 1 - Important step'
 * step('Very important step') // => '● 2 - Very important step'
 */
function step (msg, options = {}) {
  stepsCache.index++
  if (options.index === undefined) {
    options.index = stepsCache.index
  }
  log(`${options.index !== null ? (options.index + ' - ') : ''}${msg}`, { indentChar: '● ' })
}

/**
 * @alias utils.substep
 * @param {string} msg - The message to display
 * @param {object} [options] - Options object
 * @param {boolean} [options.last] - Defines if it is the last substep
 * @returns {void}
 * @description
 * Log substep title
 * @example
 * const { substep } = require('@hperchec/juisy').utils
 * substep('Awesome substep') // => '├ Awesome substep'
 * substep('Last substep', { last: true }) // => '└ Last substep'
 */
function substep (msg, options = {}) {
  log(msg, { indentChar: options.last ? '└ ' : '├ ' })
}

/**
 * @alias utils.warn
 * @param {string} msg - The message to display
 * @returns {void}
 * @description
 * Display warning message
 * @example
 * const { warn } = require('@hperchec/juisy').utils
 * warn('No configuration file') // => '[33m⚠ Warning: No configuration file[0m'
 */
function warn (msg) {
  log($style.yellow(`⚠ Warning: ${msg}`))
}

/**
 * @alias utils.error
 * @param {string} msg - The message to display
 * @param {Error} [err] - If provided, throw the error
 * @returns {void}
 * @description
 * Display error message. Throws `err` if provided.
 * @example
 * const { error } = require('@hperchec/juisy').utils
 * error('No configuration file') // => '[31m⨉ ERROR: No configuration file[0m'
 * error('No configuration file', error) // => Log and throws error
 */
function error (msg, err = undefined) {
  log($style.red(`⨉ ERROR: ${msg}`))
  if (err !== undefined) {
    log() // blank line
    throw err
  }
}

/**
 * @alias utils.abort
 * @param {number} [code] - Code for process.exit() (default: 0)
 * @returns {void}
 * @description
 * Exit process
 * @example
 * const { abort } = require('@hperchec/juisy').utils
 * abort() // => exit process with code 0
 * abort(1) // => error code
 */
function abort (code = 0) {
  log($style.yellow('Aborted...'))
  process.exit(code)
}

/**
 * @alias utils.confirm
 * @param {prompts.PromptObject} question - A prompt question object (see https://gitlab.com/hperchec/juisy/-/blob/main/documentation/utils.md#utilspromptsargs-object)
 * @returns {Promise<boolean>} - True if confirmed
 * @description
 * Demand confirmation with prompts native util. If not confirmed, it will automatically abort the script.
 * @example
 * confirm({ message: 'Confirm to continue' }) // Deny it will abort the script
 */
async function confirm (question) {
  // If undefined provided, set as empty object
  if (!question) { question = {} }
  // Wait for user prompt
  const { yes } = await prompts([
    {
      message: 'Confirm?',
      initial: true,
      ...question,
      type: 'confirm',
      name: 'yes'
    }
  ])
  if (!yes) {
    abort() // exit if not confirmed
  } else {
    return true
  }
}

/**
 * @alias utils
 * @type {object}
 * @description From: @hperchec/juisy@{{{{VERSION}}}}
 * @example
 * const { utils } = require('@hperchec/juisy')
 */
module.exports = {
  rootDir,
  packageJson,
  $style,
  merge,
  getFileUrls,
  getFiles,
  wait,
  run,
  log,
  step,
  substep,
  indent,
  warn,
  error,
  abort,
  confirm,
  /**
   * @alias utils.prompts
   * @function
   * @param {...*} args - `prompts` arguments
   * @returns {object}
   * @description
   * See `prompts` documentation
   * @example
   * const { prompts } = require('@hperchec/juisy').utils
   * // See prompts documentation
   */
  prompts,
  /**
   * @alias utils.stripAnsi
   * @function
   * @param {...*} args - `strip-ansi` arguments
   * @returns {string}
   * @description
   * See `strip-ansi` documentation
   * @example
   * const { stripAnsi } = require('@hperchec/juisy').utils
   * // See strip-ansi documentation
   */
  stripAnsi
}
