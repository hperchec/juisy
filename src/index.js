/**
 * @file Entry point
 * @description @hperchec/juisy [/src/index.js]
 * @author Hervé Perchec
 */

/**
 * @module juisy
 * @type {object}
 * @typicalname juisy
 * @description
 * From: @hperchec/juisy@{{{{VERSION}}}}
 * @example
 * const juisy = require('@hperchec/juisy')
 */

const getCallerFile = require('get-caller-file')
const Yargs = require('yargs/yargs')
const utils = require('./utils')

/**
 * @param {Function} builder - The CLI builder function
 * @returns {Function} A function that takes optional argv as first parameter and returns {@link CLI CLI}
 * @description
 * Creates a CLI (yargs instance) factory
 * @example
 * const { createCli } = require('@hperchec/juisy')
 *
 * const cli = createCli(cli => cli
 *   .scriptName('my-juicy-cli')
 *   .usage('Usage: $0 <command> [<options>]')
 * )
 *
 * const argv = process.argv.slice(2)
 *
 * // Parse argv
 * cli().parse(argv)
 */
exports.createCli = function (builder) {
  return function (argv) {
    const cli = initYargs(Yargs(argv))
    return builder(cli)
  }
}

/**
 * @typedef {object} CommandDoclet
 * @category types
 * @description
 * Command documentation object returned by {@link module:juisy.cliTools.extractUsage extractUsage} tool.
 * @property {string} command - The command
 * @property {string[]} args - The command args
 * @property {object} aliases - The yargs instance `aliasMap` reference
 * @property {object} deprecated - If command is deprecated
 * @property {module:juisy~ExtractedUsage} extractedUsage - The extracted usage object from yargs usage instance
 * @property {string} rawUsage - The usage output as string (as if `--help` option passed)
 * @property {?object} [children] - The child commands
 */

/**
 * @typedef {object} ExtractedUsage
 * @category types
 * @description
 * Type of {@link module:juisy~CommandDoclet CommandDoclet} `extractedUsage` property
 * @property {object} demandedCommands - Same as yargs instance `getDemandedCommands` method
 * @property {object} demandedOptions - Same as yargs instance `getDemandedOptions` method
 * @property {object} deprecatedOptions - Same as yargs instance `getDeprecatedOptions` method
 * @property {object} groups - Same as yargs instance `getGroups` method
 * @property {object} options - Same as yargs instance `getOptions` method
 * @property {string[]} usages - Same as yargs usage instance `getUsage` method
 * @property {object} commands - The result of yargs usage instance `getCommands` method as object.
 * While the UsageInstance commands items are defined as follow: `[cmd, description, isDefault, aliases, deprecated]`,
 * we cast it to an object like: `{cmd, description, isDefault, aliases, deprecated}`.
 */

/**
 * @alias module:juisy.cliTools
 * @type {object}
 * @description
 * CLI tools
 */
const cliTools = exports.cliTools = {
  /**
   * @async
   * @param {Function} cliFactory - The cli factory function returned by createCli method
   * @param {boolean} [recursive = false] - Recursive mode (parse child commands)
   * @param {string[]} [args = [""]] - The base args to pass as parseAsync first parameter
   * @param {string} [locale = en] - The locale
   * @returns {Promise<module:juisy~CommandDoclet>} The extracted command doclet
   * @fulfil {module:juisy~CommandDoclet}
   * @description
   * Extract usage object from CLI (Yargs) instance factory
   * @example
   * **Note**: don't call `cli()`, pass the factory.
   *
   * ```js
   * const { createCli, cliTools } = require('@hperchec/juisy')
   *
   * const cli = createCli(cli => cli
   *   // ...
   * )
   *
   * cliTools.extractUsage(cli)
   * ```
   *
   * Recursively extract usage:
   *
   * ```js
   * await cliTools.extractUsage(cli, true)
   * ```
   * With base args:
   *
   * ```js
   * await cliTools.extractUsage(cli, false, [ 'my-command' ])
   * ```
   *
   * Change locale:
   *
   * ```js
   * await cliTools.extractUsage(cli, undefined, undefined, 'fr')
   * ```
   */
  async extractUsage (cliFactory, recursive = false, args = [ '' ], locale = 'en') {
    const innerYargs = cliFactory([ '' ]) // empty argv
    // Set locale
    innerYargs.locale(locale)

    // Init doclet object extracted usage object
    const doclet = {
      command: undefined,
      args: undefined,
      aliases: undefined,
      // deprecated: false,
      extractedUsage: undefined,
      rawUsage: undefined,
      children: recursive ? {} : undefined
    }

    const parseCallback = function (err, argv, output) {
      if (err) throw err
      // this is the yargs instance with temporary parse state
      const self = this
      // Set doclet args
      doclet.args = argv._
      // Set doclet command
      doclet.command = argv._[argv._.length - 1] // is there a better way?
      // Set rawUsage
      doclet.rawUsage = output
      // Extracted usage
      const extractedUsage = doclet.extractedUsage = {}

      extractedUsage.demandedCommands = self.getDemandedCommands()
      extractedUsage.demandedOptions = self.getDemandedOptions()
      extractedUsage.deprecatedOptions = self.getDeprecatedOptions()
      extractedUsage.groups = self.getGroups()
      extractedUsage.options = self.getOptions()
      // extractedUsage.examples = examples // @todo: No API for UsageInstance examples?

      const internalMethods = self.getInternalMethods()
      const usageInstance = internalMethods.getUsageInstance()

      extractedUsage.usages = usageInstance.getUsage()

      const commandInstance = internalMethods.getCommandInstance()

      // Set doclet aliases
      doclet.aliases = commandInstance.aliasMap

      const childCommandsFromHandlers = commandInstance.handlers
      const childCommandsFromUsage = usageInstance.getCommands()

      extractedUsage.commands = Object.keys(childCommandsFromHandlers).reduce((accumulator, handlerKey) => {
        // the UsageInstance commands item is like: [cmd, description, isDefault, aliases, deprecated]
        const [ cmd, description, isDefault, aliases, deprecated ] = childCommandsFromUsage.find((command) => {
          return command[0] === childCommandsFromHandlers[handlerKey].original
        })
        accumulator[handlerKey] = { cmd, description, isDefault, aliases, deprecated }
        return accumulator
      }, {})
    }

    // Parse async command
    const parsed = await innerYargs.parseAsync(args, { help: true }, parseCallback) // eslint-disable-line no-unused-vars

    // If recursive, loop on child commands
    if (recursive) {
      for (const childCommand in doclet.extractedUsage.commands) {
        // Recursively call extractUsage method
        doclet.children[childCommand] = await cliTools.extractUsage(
          cliFactory,
          true,
          [ ...(args.filter(a => a !== '')), childCommand ],
          locale
        )
      }
    }

    return doclet
  }
}

/**
 * @type {object}
 * @description
 * Juisy utils. See [utils](./utils.md) documentation.
 */
exports.utils = utils

/**
 * @ignore
 * @param {Yargs} yargs - The yargs instance
 * @returns {CLI} The initialized yargs as CLI
 */
function initYargs (yargs) {
  // Private properties
  yargs._isPrivate = false
  yargs._globalCommandVisitors = {}
  yargs._globalCommandVisitorsEnabled = new Set([])
  yargs._meta = {}

  // Methods
  const originalCommandDirMethod = yargs.commandDir
  yargs.originalCommandDir = originalCommandDirMethod
  yargs.commandDir = commandDir
  yargs.globalCommandVisitor = globalCommandVisitor
  yargs.globalCommandVisitorOptions = globalCommandVisitorOptions
  yargs.hasGlobalCommandVisitor = hasGlobalCommandVisitor
  yargs.disableGlobalCommandVisitors = disableGlobalCommandVisitors
  yargs.enableGlobalCommandVisitors = enableGlobalCommandVisitors
  yargs.isPrivate = isPrivate
  yargs.getMeta = getMeta

  return yargs
    .globalCommandVisitor('private-command-visitor', privateCommandVisitor, privateCommandVisitor.options)
    .globalCommandVisitor('get-meta-command-visitor', getMetaCommandVisitor, getMetaCommandVisitor.options)
    .strict() // disallow unknow command
    .help() // Enable help
}

/**
 * @typedef {Yargs} CLI
 * @typicalname cli
 * @category types
 * @description
 * CLI object returned by cli factory from {@link module:juisy.createCli createCli} method.
 *
 * It has all inherited properties and methods from [yargs](https://github.com/yargs/yargs) instance plus the following:
 */

/**
 * @alias module:juisy~CLI#commandDir
 * @param {string} dir - Same as yargs `commandDir` argument
 * @param {object} [options] - Same as yargs `commandDir` options
 * @returns {CLI} Returns the CLI (yargs) instance
 * @description
 * See [yargs.commandDir](https://yargs.js.org/docs/#api-reference-commanddirdirectory-opts) documentation.
 */
function commandDir (dir, options = {}) {
  const self = this // this is yargs instance
  // Is options.visit set?
  const visitor = options.visit || (cmdObj => cmdObj)

  // Add directory with wrapped visit option
  self.getInternalMethods().getCommandInstance().addDirectory(dir, require, getCallerFile(), {
    ...options,
    visit: function (commandObject, pathToFile, filename) {
      let failOnFalsyReturn
      // Loop on enabled global command visitors
      for (const name of self._globalCommandVisitorsEnabled) {
        const globalVisitor = self._globalCommandVisitors[name]
        // Call global visitor: fail on falsy returned value
        if (!globalVisitor(commandObject, pathToFile, filename, self, globalVisitor.options)) {
          failOnFalsyReturn = true
        }
      }

      // Then custom visitor
      const customVisitorResult = visitor(commandObject, pathToFile, filename, self)

      return failOnFalsyReturn ? false : customVisitorResult
    }
  })
  return self
}

/**
 * @callback module:juisy~CLI~globalCommandVisitorCallback
 * @category callback
 * @param {object} commandObject - The command object
 * @param {string} pathToFile - The path to file
 * @param {string} filename - The filename
 * @param {CLI} yargs - The current CLI instance
 * @param {object} options - The default options to set
 * @description
 * Custom global command visitor callback.
 *
 * Takes same parameters as original yargs commandDir "visit" option, plus yargs itself and visitor options.
 */

/**
 * @alias module:juisy~CLI#globalCommandVisitor
 * @param {string} name - The visitor unique name
 * @param {module:juisy~CLI~globalCommandVisitorCallback} visitor - The visitor function
 * @param {object} defaultOptions - The default visitor options object
 * @returns {CLI} Returns the CLI (yargs) instance
 * @description
 * Set a global command visitor function
 * @example
 * const visitor = function (commandObject, pathToFile, filename, yargs, options) {
 *   const { foo } = options
 *   console.log('The "foo" option : ', foo)
 *   return commandObject
 * }
 *
 * cli.globalCommandVisitor('my-custom-visitor', visitor, { foo: 'bar' })
 *   .commandDir('cmds_dir')
 * // => will console log foo option for each command visited
 */
function globalCommandVisitor (name, visitor, defaultOptions = {}) {
  this._globalCommandVisitors[name] = visitor
  return this.enableGlobalCommandVisitors([ name ])
    .globalCommandVisitorOptions(name, defaultOptions)
}

/**
 * @alias module:juisy~CLI#globalCommandVisitorOptions
 * @param {string} name - The visitor unique name
 * @param {object} options - The visitor options object
 * @returns {CLI} Returns the CLI (yargs) instance
 * @description
 * Set global command visitor options
 * @example
 * // Overrides default options for 'my-custom-visitor'
 * // defined via globalCommandVisitor method
 * cli.globalCommandVisitorOptions('my-custom-visitor', { foo: 'baz' })
 */
function globalCommandVisitorOptions (name, options) {
  this._globalCommandVisitors[name].options = {
    ...this._globalCommandVisitors[name].options,
    ...options
  }
  return this
}

/**
 * @alias module:juisy~CLI#hasGlobalCommandVisitor
 * @param {string} name - The visitor name
 * @returns {boolean} True if global command visitor is defined
 * @description
 * Check if global command visitor is defined
 * @example
 * cli.hasGlobalCommandVisitor('my-custom-visitor')
 */
function hasGlobalCommandVisitor (name) {
  return Object.keys(this._globalCommandVisitors).includes(name)
}

/**
 * @alias module:juisy~CLI#disableGlobalCommandVisitors
 * @param {string[]} visitors - The visitor names
 * @returns {CLI} Returns the CLI (yargs) instance
 * @description
 * Disables global command visitors
 * @example
 * // Disables the "my-disabled-visitor" global command visitor
 * cli.disableGlobalCommandVisitors(['my-disabled-visitor'])
 */
function disableGlobalCommandVisitors (visitors = []) {
  for (const name of visitors) {
    // If visitor exists
    if (this.hasGlobalCommandVisitor(name)) {
      // Enable
      this._globalCommandVisitorsEnabled.delete(name)
    } else {
      throw new Error('Global command visitor: "' + name + '" not defined.')
    }
  }
  return this
}

/**
 * @alias module:juisy~CLI#enableGlobalCommandVisitors
 * @param {string[]} visitors - The visitor names
 * @returns {CLI} Returns the CLI (yargs) instance
 * @description
 * Enables global command visitors
 * @example
 * // Enables the "my-custom-visitor" and "my-awesome-visitor"
 * // global command visitors
 * cli.enableGlobalCommandVisitors(['my-custom-visitor', 'my-awesome-visitor'])
 */
function enableGlobalCommandVisitors (visitors = []) {
  for (const name of visitors) {
    // If visitor exists
    if (this.hasGlobalCommandVisitor(name)) {
      // Enable
      this._globalCommandVisitorsEnabled.add(name)
    } else {
      throw new Error('Global command visitor: "' + name + '" not defined.')
    }
  }
  return this
}

/**
 * @alias module:juisy~CLI#isPrivate
 * @returns {boolean} True if command is private
 * @description
 * Get command private status
 */
function isPrivate () {
  return this._isPrivate
}

/**
 * @alias module:juisy~CLI#getMeta
 * @returns {object} The command meta
 * @description
 * Get the CLI instance meta
 * @example
 * cli.getMeta()
 */
function getMeta () {
  return this._meta
}

/**
 * @ignore
 * @param {?Function} target - The target builder
 * @param {Function} builder - The wrap
 * @description
 * Wrap command builder (target) with builder passed as second parameter
 * that will be called before target. Target can be undefined
 */
function wrapBuilder (target, builder) {
  // Is target set?
  const _target = target || (yargs => yargs)
  // Return wrapped builder
  return function (yargs) {
    return _target(builder(yargs))
  }
}

/**
 * @ignore
 * @param {object} commandObject - Same as yargs.commandDir "visit" option
 * @param {string} pathToFile - Same as yargs.commandDir "visit" option
 * @param {string} filename - Same as yargs.commandDir "visit" option
 * @param {CLI} cli - The CLI (yargs) instance
 * @param {object} options - The global command visitor defined options
 * @returns {object|boolean} False if command is private and env key is set to "private".
 * Otherwise, returns the commandObject with wrapped builder to tag CLI instance as private.
 * @description
 * Global command visitor to auto-set command private status
 */
function privateCommandVisitor (commandObject, pathToFile, filename, cli, options) {
  const { cmdObjectProp, envKey } = options
  if (commandObject[cmdObjectProp]) {
    // Check if env key is set to 'private'
    if (process.env[envKey] === 'private') {
      // Wrap builder
      commandObject.builder = wrapBuilder(commandObject.builder, function (yargs) {
        yargs._isPrivate = true
        return yargs
      })
    } else {
      // Else, don't return command object
      return false
    }
  }
  return commandObject
}

privateCommandVisitor.options = {
  cmdObjectProp: 'private',
  envKey: 'CLI_ENV'
}

/**
 * @ignore
 * @param {object} commandObject - Same as yargs.commandDir "visit" option
 * @param {string} pathToFile - Same as yargs.commandDir "visit" option
 * @param {string} filename - Same as yargs.commandDir "visit" option
 * @param {CLI} cli - The CLI (yargs) instance
 * @param {object} options - The global command visitor defined options
 * @returns {object} The commandObject with wrapped builder to inject CLI instance meta.
 * @description
 * Global command visitor to auto-set command meta
 */
function getMetaCommandVisitor (commandObject, pathToFile, filename, cli, options) {
  const { cmdObjectProp } = options
  // if prop is defined (default: "meta")
  if (commandObject[cmdObjectProp]) {
    const commandMeta = commandObject[cmdObjectProp]
    // Wrap builder
    commandObject.builder = wrapBuilder(commandObject.builder, function (yargs) {
      // Deep merge the meta objects with parent
      yargs._meta = utils.merge(yargs._meta, commandMeta)
      return yargs
    })
  }
  return commandObject
}

getMetaCommandVisitor.options = {
  cmdObjectProp: 'meta'
}
