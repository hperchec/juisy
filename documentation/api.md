# API documentation

<a name="module_juisy"></a>

## juisy : <code>object</code>
From: @hperchec/juisy@1.4.0

**Example**  
```js
const juisy = require('@hperchec/juisy')
```

* [juisy](#module_juisy) : <code>object</code>
    * _static_
        * [.utils](#module_juisy.utils) : <code>object</code>
        * [.cliTools](#module_juisy.cliTools) : <code>object</code>
            * [.extractUsage(cliFactory, [recursive], [args], [locale])](#module_juisy.cliTools.extractUsage) ⇒ [<code>Promise.&lt;CommandDoclet&gt;</code>](#module_juisy..CommandDoclet)
        * [.createCli(builder)](#module_juisy.createCli) ⇒ <code>function</code>
    * _inner_
        * _types_
            * [~CommandDoclet](#module_juisy..CommandDoclet) : <code>object</code>
            * [~ExtractedUsage](#module_juisy..ExtractedUsage) : <code>object</code>
            * [~CLI](#module_juisy..CLI) : <code>Yargs</code>
                * _instance_
                    * [.commandDir(dir, [options])](#module_juisy..CLI+commandDir) ⇒ <code>CLI</code>
                    * [.globalCommandVisitor(name, visitor, defaultOptions)](#module_juisy..CLI+globalCommandVisitor) ⇒ <code>CLI</code>
                    * [.globalCommandVisitorOptions(name, options)](#module_juisy..CLI+globalCommandVisitorOptions) ⇒ <code>CLI</code>
                    * [.hasGlobalCommandVisitor(name)](#module_juisy..CLI+hasGlobalCommandVisitor) ⇒ <code>boolean</code>
                    * [.disableGlobalCommandVisitors(visitors)](#module_juisy..CLI+disableGlobalCommandVisitors) ⇒ <code>CLI</code>
                    * [.enableGlobalCommandVisitors(visitors)](#module_juisy..CLI+enableGlobalCommandVisitors) ⇒ <code>CLI</code>
                    * [.isPrivate()](#module_juisy..CLI+isPrivate) ⇒ <code>boolean</code>
                    * [.getMeta()](#module_juisy..CLI+getMeta) ⇒ <code>object</code>
                * _inner_
                    * _callback_
                        * [~globalCommandVisitorCallback](#module_juisy..CLI..globalCommandVisitorCallback) : <code>function</code>

<a name="module_juisy.utils"></a>

### juisy.utils : <code>object</code>
Juisy utils. See [utils](./utils.md) documentation.

<a name="module_juisy.cliTools"></a>

### juisy.cliTools : <code>object</code>
CLI tools

<a name="module_juisy.cliTools.extractUsage"></a>

#### cliTools.extractUsage(cliFactory, [recursive], [args], [locale]) ⇒ [<code>Promise.&lt;CommandDoclet&gt;</code>](#module_juisy..CommandDoclet)
Extract usage object from CLI (Yargs) instance factory

**Returns**: [<code>Promise.&lt;CommandDoclet&gt;</code>](#module_juisy..CommandDoclet) - The extracted command doclet  
**Fulfil**: [<code>CommandDoclet</code>](#module_juisy..CommandDoclet)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| cliFactory | <code>function</code> |  | The cli factory function returned by createCli method |
| [recursive] | <code>boolean</code> | <code>false</code> | Recursive mode (parse child commands) |
| [args] | <code>Array.&lt;string&gt;</code> | <code>[&quot;&quot;]</code> | The base args to pass as parseAsync first parameter |
| [locale] | <code>string</code> | <code>&quot;en&quot;</code> | The locale |

**Example**  
**Note**: don't call `cli()`, pass the factory.

```js
const { createCli, cliTools } = require('@hperchec/juisy')

const cli = createCli(cli => cli
  // ...
)

cliTools.extractUsage(cli)
```

Recursively extract usage:

```js
await cliTools.extractUsage(cli, true)
```
With base args:

```js
await cliTools.extractUsage(cli, false, [ 'my-command' ])
```

Change locale:

```js
await cliTools.extractUsage(cli, undefined, undefined, 'fr')
```
<a name="module_juisy.createCli"></a>

### juisy.createCli(builder) ⇒ <code>function</code>
Creates a CLI (yargs instance) factory

**Returns**: <code>function</code> - A function that takes optional argv as first parameter and returns [CLI](CLI)  

| Param | Type | Description |
| --- | --- | --- |
| builder | <code>function</code> | The CLI builder function |

**Example**  
```js
const { createCli } = require('@hperchec/juisy')

const cli = createCli(cli => cli
  .scriptName('my-juicy-cli')
  .usage('Usage: $0 <command> [<options>]')
)

const argv = process.argv.slice(2)

// Parse argv
cli().parse(argv)
```
<a name="module_juisy..CommandDoclet"></a>

### juisy~CommandDoclet : <code>object</code>
Command documentation object returned by [extractUsage](#module_juisy.cliTools.extractUsage) tool.

**Category**: types  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| command | <code>string</code> | The command |
| args | <code>Array.&lt;string&gt;</code> | The command args |
| aliases | <code>object</code> | The yargs instance `aliasMap` reference |
| deprecated | <code>object</code> | If command is deprecated |
| extractedUsage | [<code>ExtractedUsage</code>](#module_juisy..ExtractedUsage) | The extracted usage object from yargs usage instance |
| rawUsage | <code>string</code> | The usage output as string (as if `--help` option passed) |
| [children] | <code>object</code> | The child commands |

<a name="module_juisy..ExtractedUsage"></a>

### juisy~ExtractedUsage : <code>object</code>
Type of [CommandDoclet](#module_juisy..CommandDoclet) `extractedUsage` property

**Category**: types  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| demandedCommands | <code>object</code> | Same as yargs instance `getDemandedCommands` method |
| demandedOptions | <code>object</code> | Same as yargs instance `getDemandedOptions` method |
| deprecatedOptions | <code>object</code> | Same as yargs instance `getDeprecatedOptions` method |
| groups | <code>object</code> | Same as yargs instance `getGroups` method |
| options | <code>object</code> | Same as yargs instance `getOptions` method |
| usages | <code>Array.&lt;string&gt;</code> | Same as yargs usage instance `getUsage` method |
| commands | <code>object</code> | The result of yargs usage instance `getCommands` method as object. While the UsageInstance commands items are defined as follow: `[cmd, description, isDefault, aliases, deprecated]`, we cast it to an object like: `{cmd, description, isDefault, aliases, deprecated}`. |

<a name="module_juisy..CLI"></a>

### juisy~CLI : <code>Yargs</code>
CLI object returned by cli factory from [createCli](#module_juisy.createCli) method.

It has all inherited properties and methods from [yargs](https://github.com/yargs/yargs) instance plus the following:

**Category**: types  

* [~CLI](#module_juisy..CLI) : <code>Yargs</code>
    * _instance_
        * [.commandDir(dir, [options])](#module_juisy..CLI+commandDir) ⇒ <code>CLI</code>
        * [.globalCommandVisitor(name, visitor, defaultOptions)](#module_juisy..CLI+globalCommandVisitor) ⇒ <code>CLI</code>
        * [.globalCommandVisitorOptions(name, options)](#module_juisy..CLI+globalCommandVisitorOptions) ⇒ <code>CLI</code>
        * [.hasGlobalCommandVisitor(name)](#module_juisy..CLI+hasGlobalCommandVisitor) ⇒ <code>boolean</code>
        * [.disableGlobalCommandVisitors(visitors)](#module_juisy..CLI+disableGlobalCommandVisitors) ⇒ <code>CLI</code>
        * [.enableGlobalCommandVisitors(visitors)](#module_juisy..CLI+enableGlobalCommandVisitors) ⇒ <code>CLI</code>
        * [.isPrivate()](#module_juisy..CLI+isPrivate) ⇒ <code>boolean</code>
        * [.getMeta()](#module_juisy..CLI+getMeta) ⇒ <code>object</code>
    * _inner_
        * _callback_
            * [~globalCommandVisitorCallback](#module_juisy..CLI..globalCommandVisitorCallback) : <code>function</code>

<a name="module_juisy..CLI+commandDir"></a>

#### cli.commandDir(dir, [options]) ⇒ <code>CLI</code>
See [yargs.commandDir](https://yargs.js.org/docs/#api-reference-commanddirdirectory-opts) documentation.

**Returns**: <code>CLI</code> - Returns the CLI (yargs) instance  

| Param | Type | Description |
| --- | --- | --- |
| dir | <code>string</code> | Same as yargs `commandDir` argument |
| [options] | <code>object</code> | Same as yargs `commandDir` options |

<a name="module_juisy..CLI+globalCommandVisitor"></a>

#### cli.globalCommandVisitor(name, visitor, defaultOptions) ⇒ <code>CLI</code>
Set a global command visitor function

**Returns**: <code>CLI</code> - Returns the CLI (yargs) instance  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The visitor unique name |
| visitor | [<code>globalCommandVisitorCallback</code>](#module_juisy..CLI..globalCommandVisitorCallback) | The visitor function |
| defaultOptions | <code>object</code> | The default visitor options object |

**Example**  
```js
const visitor = function (commandObject, pathToFile, filename, yargs, options) {
  const { foo } = options
  console.log('The "foo" option : ', foo)
  return commandObject
}

cli.globalCommandVisitor('my-custom-visitor', visitor, { foo: 'bar' })
  .commandDir('cmds_dir')
// => will console log foo option for each command visited
```
<a name="module_juisy..CLI+globalCommandVisitorOptions"></a>

#### cli.globalCommandVisitorOptions(name, options) ⇒ <code>CLI</code>
Set global command visitor options

**Returns**: <code>CLI</code> - Returns the CLI (yargs) instance  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The visitor unique name |
| options | <code>object</code> | The visitor options object |

**Example**  
```js
// Overrides default options for 'my-custom-visitor'
// defined via globalCommandVisitor method
cli.globalCommandVisitorOptions('my-custom-visitor', { foo: 'baz' })
```
<a name="module_juisy..CLI+hasGlobalCommandVisitor"></a>

#### cli.hasGlobalCommandVisitor(name) ⇒ <code>boolean</code>
Check if global command visitor is defined

**Returns**: <code>boolean</code> - True if global command visitor is defined  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The visitor name |

**Example**  
```js
cli.hasGlobalCommandVisitor('my-custom-visitor')
```
<a name="module_juisy..CLI+disableGlobalCommandVisitors"></a>

#### cli.disableGlobalCommandVisitors(visitors) ⇒ <code>CLI</code>
Disables global command visitors

**Returns**: <code>CLI</code> - Returns the CLI (yargs) instance  

| Param | Type | Description |
| --- | --- | --- |
| visitors | <code>Array.&lt;string&gt;</code> | The visitor names |

**Example**  
```js
// Disables the "my-disabled-visitor" global command visitor
cli.disableGlobalCommandVisitors(['my-disabled-visitor'])
```
<a name="module_juisy..CLI+enableGlobalCommandVisitors"></a>

#### cli.enableGlobalCommandVisitors(visitors) ⇒ <code>CLI</code>
Enables global command visitors

**Returns**: <code>CLI</code> - Returns the CLI (yargs) instance  

| Param | Type | Description |
| --- | --- | --- |
| visitors | <code>Array.&lt;string&gt;</code> | The visitor names |

**Example**  
```js
// Enables the "my-custom-visitor" and "my-awesome-visitor"
// global command visitors
cli.enableGlobalCommandVisitors(['my-custom-visitor', 'my-awesome-visitor'])
```
<a name="module_juisy..CLI+isPrivate"></a>

#### cli.isPrivate() ⇒ <code>boolean</code>
Get command private status

**Returns**: <code>boolean</code> - True if command is private  
<a name="module_juisy..CLI+getMeta"></a>

#### cli.getMeta() ⇒ <code>object</code>
Get the CLI instance meta

**Returns**: <code>object</code> - The command meta  
**Example**  
```js
cli.getMeta()
```
<a name="module_juisy..CLI..globalCommandVisitorCallback"></a>

#### CLI~globalCommandVisitorCallback : <code>function</code>
Custom global command visitor callback.

Takes same parameters as original yargs commandDir "visit" option, plus yargs itself and visitor options.

**Category**: callback  

| Param | Type | Description |
| --- | --- | --- |
| commandObject | <code>object</code> | The command object |
| pathToFile | <code>string</code> | The path to file |
| filename | <code>string</code> | The filename |
| yargs | <code>CLI</code> | The current CLI instance |
| options | <code>object</code> | The default options to set |

