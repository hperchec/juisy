# Utils documentation

<a name="utils"></a>

## utils : <code>object</code>
From: @hperchec/juisy@1.4.0

**Example**  
```js
const { utils } = require('@hperchec/juisy')
```

* [utils](#utils) : <code>object</code>
    * [.rootPath](#utils.rootPath) : <code>string</code>
    * [.packageJson](#utils.packageJson) : <code>object</code>
    * [.$style](#utils.$style) : <code>object</code>
    * [.merge(...args)](#utils.merge) ⇒ <code>object</code>
    * [.getFileUrls(pattern, options)](#utils.getFileUrls) ⇒ <code>Array.&lt;string&gt;</code>
    * [.getFiles(dir)](#utils.getFiles) ⇒ <code>Array.&lt;string&gt;</code>
    * [.wait(message, fct)](#utils.wait) ⇒ <code>Promise.&lt;void&gt;</code>
    * [.run(bin, args, [opts])](#utils.run) ⇒ <code>Promise.&lt;object&gt;</code>
    * [.log(msg, [options])](#utils.log) ⇒ <code>void</code>
    * [.step(msg, [options])](#utils.step) ⇒ <code>void</code>
    * [.substep(msg, [options])](#utils.substep) ⇒ <code>void</code>
    * [.warn(msg)](#utils.warn) ⇒ <code>void</code>
    * [.error(msg, [err])](#utils.error) ⇒ <code>void</code>
    * [.abort([code])](#utils.abort) ⇒ <code>void</code>
    * [.confirm(question)](#utils.confirm) ⇒ <code>Promise.&lt;boolean&gt;</code>
    * [.prompts(...args)](#utils.prompts) ⇒ <code>object</code>
    * [.stripAnsi(...args)](#utils.stripAnsi) ⇒ <code>string</code>

<a name="utils.rootPath"></a>

### utils.rootPath : <code>string</code>
Get root directory path

**Example**  
```js
const { rootDir } = require('@hperchec/juisy').utils
console.log(rootDir) // => 'path/to/your/root/dir'
```
<a name="utils.packageJson"></a>

### utils.packageJson : <code>object</code>
Get package.json content as object

**Example**  
```js
const { packageJson } = require('@hperchec/juisy').utils
console.log(packageJson) // => { ... }
```
<a name="utils.$style"></a>

### utils.$style : <code>object</code>
Use `chalk` package to style output in console/stdout

**Example**  
```js
const { $style } = require('@hperchec/juisy').utils
console.log($style.green('Green text!')) // => '[32mGreen text![0m'
```
<a name="utils.merge"></a>

### utils.merge(...args) ⇒ <code>object</code>
Deep merge two objects (see: https://www.npmjs.com/package/deepmerge)

**Returns**: <code>object</code> - Returns merged object, see deepmerge documentation  

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>any</code> | Same as deepmerge arguments |

**Example**  
```js
const { merge } = require('@hperchec/juisy').utils
merge({ a: 'foo', b: 'bar' }, { a: 'fuu', c: 'bax' }) // => { a: 'fuu', b: 'bar', c: 'bax' }
```
<a name="utils.getFileUrls"></a>

### utils.getFileUrls(pattern, options) ⇒ <code>Array.&lt;string&gt;</code>
Get file urls that match glob (see: https://github.com/isaacs/node-glob)

**Returns**: <code>Array.&lt;string&gt;</code> - Returns an array of file path  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | Same as node-glob pattern argument |
| options | <code>object</code> | Same as node-glob pattern argument |

**Example**  
```js
const { getFileUrls } = require('@hperchec/juisy').utils
getFileUrls('path/to/*.js') // => [ 'path/to/file1.js', 'path/to/file2.js', ... ]
```
<a name="utils.getFiles"></a>

### utils.getFiles(dir) ⇒ <code>Array.&lt;string&gt;</code>
Get all files in a directory (recursively)

**Returns**: <code>Array.&lt;string&gt;</code> - - Returns an array of file path  

| Param | Type | Description |
| --- | --- | --- |
| dir | <code>string</code> | The directory path |

**Example**  
```js
const { getFiles } = require('@hperchec/juisy').utils
getFiles(path) // => [ 'path/to/file1', 'path/to/file2', ... ]
```
<a name="utils.wait"></a>

### utils.wait(message, fct) ⇒ <code>Promise.&lt;void&gt;</code>
Wait function: display loading during 'fct' execution


| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to display |
| fct | <code>function</code> | The function to execute |

**Example**  
```js
const { wait } = require('@hperchec/juisy').utils
await wait('Waiting', async () => {
  // Do async logic
})
```
<a name="utils.run"></a>

### utils.run(bin, args, [opts]) ⇒ <code>Promise.&lt;object&gt;</code>
Run command (child_process). See also `execa` package documentation

**Returns**: <code>Promise.&lt;object&gt;</code> - The `execa` Promise  

| Param | Type | Description |
| --- | --- | --- |
| bin | <code>string</code> | Command |
| args | <code>Array.&lt;string&gt;</code> | Same as execa second arg |
| [opts] | <code>object</code> | Options |

**Example**  
```js
const { run } = require('@hperchec/juisy').utils
await run('npm', [ 'run', 'test' ], { stdio: 'inherit' })
```
<a name="utils.log"></a>

### utils.log(msg, [options]) ⇒ <code>void</code>
Log message in console


| Param | Type | Description |
| --- | --- | --- |
| msg | <code>string</code> | The message |
| [options] | <code>object</code> | (Optional) Options object |
| [options.indent] | <code>number</code> | How many repeat indent (see `indent-string` package) |
| [options.indentChar] | <code>string</code> | Indent character(s). If `options.indent` not provided, will be repeated only 1 time |

**Example**  
```js
const { log } = require('@hperchec/juisy').utils
log() // => blank line
log('Hello world! =)') // => 'Hello world! =)'
log('To do:', { indent: 2 }) // => '  To do:'
log('laundry\nshopping', { indentChar: '- ' }) // => '- laundry\n- shopping'
log('foo\nbar', { indent: 2, indentChar: '❤' }) // => '❤❤foo\n❤❤bar'
```
<a name="utils.step"></a>

### utils.step(msg, [options]) ⇒ <code>void</code>
Log step title


| Param | Type | Description |
| --- | --- | --- |
| msg | <code>string</code> | The message to display |
| [options] | <code>object</code> | Options object |
| [options.index] | <code>number</code> \| <code>string</code> | Custom index. If `null`, no index prepended to string |

**Example**  
```js
const { step } = require('@hperchec/juisy').utils
step('Important step') // => '● 1 - Important step'
step('Very important step') // => '● 2 - Very important step'
```
<a name="utils.substep"></a>

### utils.substep(msg, [options]) ⇒ <code>void</code>
Log substep title


| Param | Type | Description |
| --- | --- | --- |
| msg | <code>string</code> | The message to display |
| [options] | <code>object</code> | Options object |
| [options.last] | <code>boolean</code> | Defines if it is the last substep |

**Example**  
```js
const { substep } = require('@hperchec/juisy').utils
substep('Awesome substep') // => '├ Awesome substep'
substep('Last substep', { last: true }) // => '└ Last substep'
```
<a name="utils.warn"></a>

### utils.warn(msg) ⇒ <code>void</code>
Display warning message


| Param | Type | Description |
| --- | --- | --- |
| msg | <code>string</code> | The message to display |

**Example**  
```js
const { warn } = require('@hperchec/juisy').utils
warn('No configuration file') // => '[33m⚠ Warning: No configuration file[0m'
```
<a name="utils.error"></a>

### utils.error(msg, [err]) ⇒ <code>void</code>
Display error message. Throws `err` if provided.


| Param | Type | Description |
| --- | --- | --- |
| msg | <code>string</code> | The message to display |
| [err] | <code>Error</code> | If provided, throw the error |

**Example**  
```js
const { error } = require('@hperchec/juisy').utils
error('No configuration file') // => '[31m⨉ ERROR: No configuration file[0m'
error('No configuration file', error) // => Log and throws error
```
<a name="utils.abort"></a>

### utils.abort([code]) ⇒ <code>void</code>
Exit process


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [code] | <code>number</code> | <code>0</code> | Code for process.exit() (default: 0) |

**Example**  
```js
const { abort } = require('@hperchec/juisy').utils
abort() // => exit process with code 0
abort(1) // => error code
```
<a name="utils.confirm"></a>

### utils.confirm(question) ⇒ <code>Promise.&lt;boolean&gt;</code>
Demand confirmation with prompts native util. If not confirmed, it will automatically abort the script.

**Returns**: <code>Promise.&lt;boolean&gt;</code> - - True if confirmed  

| Param | Type | Description |
| --- | --- | --- |
| question | <code>prompts.PromptObject</code> | A prompt question object (see https://gitlab.com/hperchec/juisy/-/blob/main/documentation/utils.md#utilspromptsargs-object) |

**Example**  
```js
confirm({ message: 'Confirm to continue' }) // Deny it will abort the script
```
<a name="utils.prompts"></a>

### utils.prompts(...args) ⇒ <code>object</code>
See `prompts` documentation


| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | `prompts` arguments |

**Example**  
```js
const { prompts } = require('@hperchec/juisy').utils
// See prompts documentation
```
<a name="utils.stripAnsi"></a>

### utils.stripAnsi(...args) ⇒ <code>string</code>
See `strip-ansi` documentation


| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | `strip-ansi` arguments |

**Example**  
```js
const { stripAnsi } = require('@hperchec/juisy').utils
// See strip-ansi documentation
```
