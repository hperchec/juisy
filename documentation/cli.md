# CLI usage

**Table of contents**

- [`changelog`](#changelog)
- [`docs`](#docs)
  - [`docs generate:api`](#docs-generateapi)
  - [`docs generate:cli`](#docs-generatecli)
  - [`docs generate:readme`](#docs-generatereadme)
  - [`docs lint`](#docs-lint)
- [`git-hooks`](#git-hooks)
  - [`git-hooks reset`](#git-hooks-reset)
  - [`git-hooks sync`](#git-hooks-sync)
- [`release`](#release)
- [`squeeze`](#squeeze)
- [`test`](#test)

**Usage**

```console
------
@hperchec/juisy
------
Made with ❤  by Hervé Perchec <contact@herve-perchec.com>

Usage: juisy <command> [<options>]

Commands:
  juisy changelog            Generate CHANGELOG file                [deprecated]
  juisy docs <command>       Manage project documentation
  juisy git-hooks <command>  Git relative commands
  juisy release              Make a release
  juisy squeeze              Initialize project folder
  juisy test                 Run tests

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```

----


## `changelog`

```
juisy changelog

Generate CHANGELOG file

Options:
      --version  Show version number                                   [boolean]
      --help     Show help                                             [boolean]
  -i, --infile   Same as conventional-changelog option
                                              [string] [default: "CHANGELOG.md"]
```




## `docs`

```
juisy docs <command>

Manage project documentation

Commands:
  juisy docs generate:api     Generate API docs from source code
  juisy docs generate:cli     Generate CLI docs
  juisy docs generate:readme  Generate README.md
  juisy docs lint             Lint markdown with markdownlint

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```


### `docs generate:api`

```
juisy docs generate:api

Generate API docs from source code

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```




### `docs generate:cli`

```
juisy docs generate:cli

Generate CLI docs

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```




### `docs generate:readme`

```
juisy docs generate:readme

Generate README.md

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```




### `docs lint`

```
juisy docs lint

Lint markdown with markdownlint

Options:
      --version  Show version number                                   [boolean]
      --help     Show help                                             [boolean]
  -c, --config   Path to custom markdownlint config file (relative to root
                 folder)                                                [string]
  -f, --fix      Auto fix by passing --fix option to markdownlint-cli2
                                                      [boolean] [default: false]
```






## `git-hooks`

```
juisy git-hooks <command>

Git relative commands

Commands:
  juisy git-hooks reset  Reset git hooks
  juisy git-hooks sync   Sync git hooks

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```


### `git-hooks reset`

```
juisy git-hooks reset

Reset git hooks

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```




### `git-hooks sync`

```
juisy git-hooks sync

Sync git hooks

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```






## `release`

```
juisy release

Make a release

Options:
      --version  Show version number                                   [boolean]
      --help     Show help                                             [boolean]
  -p, --preid    Pre-release id                                         [string]
```




## `squeeze`

```
juisy squeeze

Initialize project folder

Options:
      --version  Show version number                                   [boolean]
      --help     Show help                                             [boolean]
  -f, --force    Overwrites existing files / scripts / ...
                                                      [boolean] [default: false]
  -t, --type     Choose project type             [choices: "commonjs", "module"]
```




## `test`

```
juisy test

Run tests

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```





----

*cli.md - this file was auto generated with [@hperchec/readme-generator](https://www.npmjs.com/package/@hperchec/readme-generator). Don't edit it.*
