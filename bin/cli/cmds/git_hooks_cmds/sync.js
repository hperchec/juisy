const juisy = require('../../../../src')

const {
  rootDir,
  step,
  run
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'sync',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Sync git hooks',
  /**
   * Builder
   * @param {object} yargs - yargs
   * @returns {object} Returns yargs
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {object} argv - The argv
   * @returns {void}
   */
  handler: async function (argv) {
    /**
     * Sync git hooks
     */
    step('Sync git hooks')

    await run('npx', [ 'simple-git-hooks' ], { cwd: rootDir })
  }
}
