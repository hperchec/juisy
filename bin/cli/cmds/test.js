const path = require('path')
const juisy = require('../../../src')

// Utils
const {
  run
} = juisy.utils

const rootDir = path.resolve(__dirname, '../../../')

// Exports command object
module.exports = {
  private: true,
  /**
   * Command syntax
   */
  command: 'test',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Run tests',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    /**
     * Run tests
     */
    await run('npx', [ 'jest' ], { stdio: 'inherit', cwd: rootDir })
  }
}
