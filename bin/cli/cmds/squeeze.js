const fs = require('fs-extra')
const path = require('path')
const juisy = require('../../../src')

const {
  $style,
  rootDir: targetProjectRootDir, // rootDir is parent project
  log,
  prompts,
  step,
  substep,
  run
} = juisy.utils

const isDef = (value) => value !== undefined

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'squeeze',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Initialize project folder',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    yargs.option('f', {
      alias: 'force',
      type: 'boolean',
      default: false,
      describe: 'Overwrites existing files / scripts / ...'
    })
    yargs.option('t', {
      alias: 'type',
      choices: [ 'commonjs', 'module' ],
      describe: 'Choose project type'
    })
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    let projectType = argv.type

    if (!projectType) {
      /**
       * First, ask is target project has "type": "module" in the package.json file
       * In that case, we use the 'module' template
       */
      step('Choose project type')
      const { typePrompt } = await prompts([
        {
          type: 'select',
          name: 'typePrompt',
          message: 'If your package.json file has "type": "module" property set, use "module"',
          choices: [
            { title: 'commonjs', value: 'commonjs' },
            { title: 'module (type="module")', value: 'module' }
          ]
        }
      ])

      projectType = typePrompt
    }

    const templateDir = 'template/' + projectType
    const getTemplatePath = (target) => path.resolve(__dirname, '../../../', templateDir, target)

    /**
     * bin directory
     */
    step('Copying "bin" directory')
    // Check if 'bin' folder does not exist in destination
    const targetBinFolderPath = path.resolve(targetProjectRootDir, './bin')
    let write = fs.existsSync(targetBinFolderPath)
      ? argv.force
      : true
    if (write) {
      // Copy folder
      fs.copySync(getTemplatePath('bin'), targetBinFolderPath)
      substep($style.green(`✔ Successfuly created: ${targetBinFolderPath}`), { last: true })
    } else {
      substep($style.yellow('Target directory "bin" already exists. Skipped... Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    /**
     * .docs directory
     */
    step('Copying ".docs" directory')
    // Check if 'docs' folder does not exist in destination
    const targetDocsFolderPath = path.resolve(targetProjectRootDir, './.docs')
    write = fs.existsSync(targetDocsFolderPath)
      ? argv.force
      : true
    if (write) {
      // Copy folder
      fs.copySync(getTemplatePath('.docs'), targetDocsFolderPath)
      substep($style.green(`✔ Successfuly created: ${targetDocsFolderPath}`), { last: true })
    } else {
      substep($style.yellow('Target directory "docs" already exists. Skipped... Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    /**
     * scripts directory
     */
    step('Copying "scripts" directory')
    // Check if 'scripts' folder does not exist in destination
    const targetScriptsFolderPath = path.resolve(targetProjectRootDir, './scripts')
    write = fs.existsSync(targetScriptsFolderPath)
      ? argv.force
      : true
    if (write) {
      // Copy folder
      fs.copySync(getTemplatePath('scripts'), targetScriptsFolderPath)
      substep($style.green(`✔ Successfuly created: ${targetScriptsFolderPath}`), { last: true })
    } else {
      substep($style.yellow('Target directory "scripts" already exists. Skipped... Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    /**
     * CHANGELOG file
     */
    step('Creating CHANGELOG.md file')
    // Check if CHANGELOG exists
    const targetChangelogPath = path.resolve(targetProjectRootDir, './CHANGELOG.md')
    write = fs.existsSync(targetChangelogPath)
      ? argv.force
      : true
    if (write) {
      // Copy folder
      fs.copySync(getTemplatePath('CHANGELOG.md'), targetChangelogPath)
      substep($style.green(`✔ Successfuly created: ${targetChangelogPath}`), { last: true })
    } else {
      substep($style.yellow('Target CHANGELOG file already exists. Skipped... Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    /**
     * Update package.json file
     */
    // const scripts = {
    //   'docs:api':              'node ./bin/cli docs generate -c ./.docs/docs.config.js|cjs',
    //   'docs:readme':           'node ./bin/cli docs generate:readme',
    //   'docs:lint':             'node ./bin/cli docs lint',
    //   'docs:lint:fix':         'npm run docs:lint -- --fix',
    //   'docs':                  'npm run docs:readme && npm run docs:api && npm run docs:lint',
    //   'lint':                  'npx eslint . --ext .js',
    //   'lint:fix':              'npm run lint -- --fix',
    //   'lint:markdown':         'npm run docs:lint',
    //   'lint:markdown:fix':     'npm run docs:lint:fix',
    //   'release':               'node ./bin/cli release',
    //   'changelog':             'node ./bin/cli changelog',
    //   'git-hooks:reset':       'node ./bin/cli git-hooks reset',
    //   'git-hooks:sync':        'node ./bin/cli git-hooks sync',
    //   'test':                  'node ./bin/cli test',
    //   'postinstall':           'npm run git-hooks:sync'
    // }

    const targetCliCommand = projectType === 'module'
      ? 'node ./bin/cli/index.cjs'
      : 'node ./bin/cli'

    step('Adding scripts to package.json')
    const packageJson = require(path.resolve(targetProjectRootDir, './package.json'))
    packageJson.scripts = packageJson.scripts || {}

    // "docs:api": "node ./bin/cli docs generate:api"
    let setScript = isDef(packageJson.scripts['docs:api'])
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'docs:api', `${targetCliCommand} docs generate:api` ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "docs:api" successfuly added'))
    } else {
      substep($style.yellow('Script "docs:api" already set. Use --force option to overwrite'))
    }

    // "docs:readme": "node ./bin/cli docs generate:readme -c ./docs/readme/config.js|cjs"
    setScript = isDef(packageJson.scripts['docs:readme'])
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [
        'set-script',
        'docs:readme',
        `${targetCliCommand} docs generate:readme -c ./.docs/readme/config.${projectType === 'module' ? 'cjs' : 'js'}`
      ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "docs:readme" successfuly added'))
    } else {
      substep($style.yellow('Script "docs:readme" already set. Use --force option to overwrite'))
    }

    // "docs": "npm run docs:readme && npm run docs:api"
    setScript = isDef(packageJson.scripts.docs)
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'docs', 'npm run docs:readme && npm run docs:api' ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "docs" successfuly added'))
    } else {
      substep($style.yellow('Script "docs" already set. Use --force option to overwrite'))
    }

    // "release": "node ./bin/cli release"
    setScript = isDef(packageJson.scripts.release)
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'release', `${targetCliCommand} release` ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "release" successfuly added'))
    } else {
      substep($style.yellow('Script "release" already set. Use --force option to overwrite'))
    }

    // "changelog": "node ./bin/cli changelog"
    setScript = isDef(packageJson.scripts.changelog)
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'changelog', `${targetCliCommand} changelog` ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "changelog" successfuly added'))
    } else {
      substep($style.yellow('Script "changelog" already set. Use --force option to overwrite'))
    }

    // "git-hooks:reset": "node ./bin/cli git-hooks reset"
    setScript = isDef(packageJson.scripts['git-hooks:reset'])
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'git-hooks:reset', `${targetCliCommand} git-hooks reset` ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "git-hooks:reset" successfuly added'))
    } else {
      substep($style.yellow('Script "git-hooks:reset" already set. Use --force option to overwrite'))
    }

    // "git-hooks:sync": "node ./bin/cli git-hooks sync"
    setScript = isDef(packageJson.scripts['git-hooks:sync'])
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'git-hooks:sync', `${targetCliCommand} git-hooks sync` ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "git-hooks:sync" successfuly added'))
    } else {
      substep($style.yellow('Script "git-hooks:sync" already set. Use --force option to overwrite'))
    }

    // "postinstall": "npm run git-hooks:sync"
    setScript = isDef(packageJson.scripts.postinstall)
      ? argv.force
      : true
    if (setScript) {
      await run('npm', [ 'set-script', 'postinstall', 'npm run git-hooks:sync' ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Script "postinstall" successfuly added'))
    } else {
      substep($style.yellow('Script "postinstall" already set. Use --force option to overwrite'))
    }
    substep($style.green('✔ package.json updated'), { last: true })
    log() // Blank line

    // Adding repository info
    step('Adding repository config to package.json')
    // set repository config
    const setRepository = isDef(packageJson.repository)
      ? argv.force
      : true
    if (setRepository) {
      const repositoryConfig = {
        type: 'git',
        url: 'https://gitlab.com/hperchec/juisy'
      }
      await run('npm', [
        'pkg',
        'set',
        `repository=${JSON.stringify(repositoryConfig)}`,
        '--json'
      ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Repository config successfuly added'), { last: true })
    } else {
      substep($style.yellow('Repository config already set. Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    // Adding issues URL info
    step('Adding issues/bugs config to package.json')
    // set bugs config
    const setBugs = isDef(packageJson.bugs)
      ? argv.force
      : true
    if (setBugs) {
      const bugsConfig = {
        url: 'https://gitlab.com/hperchec/juisy/issues'
      }
      await run('npm', [
        'pkg',
        'set',
        `bugs=${JSON.stringify(bugsConfig)}`,
        '--json'
      ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Issues/bugs config successfuly added'), { last: true })
    } else {
      substep($style.yellow('Issues/bugs config already set. Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    // Adding git hooks
    step('Adding git hooks config to package.json')
    // set simple-git-hook config
    const setSimpleGitHook = isDef(packageJson['simple-git-hooks'])
      ? argv.force
      : true
    if (setSimpleGitHook) {
      const simpleGitHookConfig = {
        'pre-commit': 'node ./scripts/pre-commit.js',
        'commit-msg': 'node ./scripts/commit-msg.js ${1}' // eslint-disable-line no-template-curly-in-string
      }
      await run('npm', [
        'pkg',
        'set',
        `simple-git-hooks=${JSON.stringify(simpleGitHookConfig)}`,
        '--json'
      ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ Git hooks successfuly added'), { last: true })
    } else {
      substep($style.yellow('Git hooks already set. Use --force option to overwrite'), { last: true })
    }
    log() // Blank line

    // Adding lint-staged configured
    step('Adding lint-staged config to package.json')
    // set lint-staged config
    const setLintStaged = isDef(packageJson['lint-staged'])
      ? argv.force
      : true
    if (setLintStaged) {
      const lintStagedConfig = {
        '*.js': [
          'eslint --fix',
          'git add'
        ]
      }
      await run('npm', [
        'pkg',
        'set',
        `lint-staged=${JSON.stringify(lintStagedConfig)}`,
        '--json'
      ], { stdio: 'pipe', cwd: targetProjectRootDir })
      substep($style.green('✔ lint-staged successfuly configured'), { last: true })
    } else {
      substep($style.yellow('lint-staged already configured. Use --force option to overwrite'), { last: true })
    }
    log() // Blank line
  }
}
