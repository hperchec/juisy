const juisy = require('../../../../src')

const {
  abort,
  rootDir,
  run
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'lint',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Lint markdown with markdownlint',
  /**
   * Builder
   * @param {object} yargs - yargs
   * @returns {object} Returns yargs
   */
  builder: function (yargs) {
    yargs.option('c', {
      alias: 'config',
      type: 'string',
      describe: 'Path to custom markdownlint config file (relative to root folder)',
      requiresArg: true
    })
    yargs.option('f', {
      alias: 'fix',
      type: 'boolean',
      describe: 'Auto fix by passing --fix option to markdownlint-cli2',
      default: false
    })
    return yargs
  },
  /**
   * Handler
   * @param {object} argv - The argv
   * @returns {void}
   */
  handler: async function (argv) {
    const configPath = argv.config || './.docs/.markdownlint-cli2.cjs' // default relative to root folder

    /**
     * Call markdownlint command
     */
    try {
      await run(
        'npx',
        [
          'markdownlint-cli2',
          '--config',
          configPath,
          ...(argv.fix ? [ '--fix' ] : [])
        ],
        { stdio: 'inherit', cwd: rootDir }
      )
    } catch (error) {
      abort(error.exitCode)
    }
  }
}
