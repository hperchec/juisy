const path = require('path')
const { generate: readmeGenerator } = require('@hperchec/readme-generator')
const { utils } = require('../../../../src')

const { rootDir } = utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'generate:cli',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate CLI docs',
  /**
   * Builder
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    // readme-generator config path
    const config = path.resolve(rootDir, './.docs/cli/config.js')
    // Call generator with config
    await readmeGenerator(config)
  }
}
