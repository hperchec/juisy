const path = require('path')
const juisy = require('../../../../src')

const {
  rootDir
} = juisy.utils

const generateApiDoc = require('../../lib/docs/generate-api-doc')

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'generate:api',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate API docs from source code',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    let options = {}
    const configPath = path.resolve(rootDir, './.docs/api/docs.config.js')

    options = require(configPath)

    /**
     * If everything is okay generate doc
     */
    await generateApiDoc(options)
  }
}
