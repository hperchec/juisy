const { generate: readmeGenerator } = require('@hperchec/readme-generator')

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'generate:readme',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate README.md',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    // Call generator (default configuration)
    await readmeGenerator('./.docs/readme/config.js')
  }
}
