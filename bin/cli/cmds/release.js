const path = require('path')
const semver = require('semver')
const juisy = require('../../../src')

const {
  $style,
  log,
  step,
  substep,
  run,
  error,
  wait,
  confirm,
  prompts
} = juisy.utils

const rootDir = path.resolve(__dirname, '../../../')

// Get package.json content
const packageJson = require(path.resolve(rootDir, './package.json'))
const updateVersion = require('../lib/version/update-version')

// Exports command object
module.exports = {
  private: true,
  /**
   * Command syntax
   */
  command: 'release',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Make a release',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs.option('p', {
      alias: 'preid',
      type: 'string',
      describe: 'Pre-release id',
      requiresArg: true
    })
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    let targetVersion
    const currentVersion = packageJson.version
    const packageName = packageJson.name
    const preId = argv.preid || (semver.prerelease(currentVersion) && semver.prerelease(currentVersion)[0])
    const inc = i => semver.inc(currentVersion, i, preId)
    const versionIncrements = [
      'patch',
      'minor',
      'major',
      ...(preId ? [ 'prepatch', 'preminor', 'premajor', 'prerelease' ] : [])
    ]

    /**
     * First, check if local repository is clean
     */
    step('Checking changes to commit')
    const { stdout } = await run('git', [ 'diff' ], { stdio: 'pipe', cwd: rootDir })
    if (stdout) {
      error('Please commit your changes before creating a new release!', new Error('There are changes to commit'))
    }
    substep($style.green('✔ Local repository is clean'), { last: true })
    log() // Blank line

    /**
     * Release prompt
     */
    step('Setup')
    const { release } = await prompts([
      {
        type: 'select',
        name: 'release',
        message: 'Release type:',
        choices: versionIncrements.map(i => ({ title: `${i} (${inc(i)})`, value: inc(i) })).concat([ { title: 'custom', value: 'custom' } ])
      }
    ])
    // If custom release
    if (release === 'custom') {
      const { version: customVersion } = await prompts([
        {
          type: 'text',
          name: 'version',
          message: 'New custom version:',
          initial: currentVersion,
          validate: value => Boolean(semver.valid(value))
        }
      ])
      targetVersion = customVersion
    } else {
      targetVersion = release
    }

    /**
     * Demand confirmation
     */
    await confirm({ message: `Releasing v${targetVersion}. Confirm?` })
    log() // Blank line

    /**
     * Run tests
     */
    // step(`Running tests`)
    // ... here run tests

    /**
     * Update version in necessary files
     */
    await updateVersion(targetVersion)
    log() // Blank line

    /**
     * Generate docs
     */
    step('Generate docs')
    await wait('API docs', async () => {
      await run('node', [ './bin/cli', 'docs', 'generate:api' ], { stdio: 'pipe', cwd: rootDir })
    })
    substep($style.green('✔ API docs successfuly generated'))
    await wait('CLI docs', async () => {
      await run('node', [ './bin/cli', 'docs', 'generate:cli' ], { stdio: 'pipe', cwd: rootDir })
    })
    substep($style.green('✔ CLI docs successfuly generated'))
    await wait('README', async () => {
      await run('node', [ './bin/cli', 'docs', 'generate:readme' ], { stdio: 'pipe', cwd: rootDir })
    })
    substep($style.green('✔ README successfuly generated'), { last: true })
    // await wait('Linting', async () => {
    //   await run('node', [ './bin/cli', 'docs', 'lint', '-c', './.docs/.markdownlint-cli2.cjs' ], { stdio: 'pipe', cwd: rootDir })
    // })
    log() // Blank line

    /**
     * Generate changelog file
     */
    step('Generating changelog')
    await wait('Generating', async () => {
      await run('node', [ './bin/cli', 'changelog' ], { stdio: 'pipe', cwd: rootDir })
    })
    substep($style.green('✔ Success'), { last: true })
    log() // Blank line

    /**
     * Confirm changes to commit
     */
    step('Confirm changes to commit')
    substep($style.yellow('↓ The following changes will be committed:'))
    await run('git', [ 'status', '-s' ], { stdio: 'inherit', cwd: rootDir })
    substep($style.yellow('- The following tag will be created: ') + `v${targetVersion}`, { last: true })

    /**
     * Demand confirmation
     */
    await confirm()
    log() // Blank line

    /**
     * Publish package
     */
    step(`Publishing ${packageName}`)
    const releaseTag = targetVersion.includes('alpha')
      ? 'alpha'
      : targetVersion.includes('beta')
        ? 'beta'
        : targetVersion.includes('rc')
          ? 'rc'
          : null
    let alreadyPublished = false
    await wait('Publishing', async () => {
      try {
        await run('npm', [ 'publish', ...(releaseTag ? [ '--tag', releaseTag ] : []) ], { stdio: 'pipe', cwd: rootDir })
      } catch (e) {
        if (e.stderr.match(/previously published/)) {
          alreadyPublished = true
        } else {
          error('Unknown error during publishing', e)
        }
      }
    })
    substep(
      alreadyPublished
        ? $style.yellow(`Skipping already published: ${packageName}`)
        : $style.green('✔ Success'),
      { last: true }
    )
    log() // Blank line

    /**
     * Push to git
     */
    step('Pushing changes')
    await wait('Committing', async () => {
      try {
        await run('git', [ 'add', '.' ], { stdio: 'pipe', cwd: rootDir })
        await run('git', [ 'commit', '-m', `chore(release): v${targetVersion}` ], { stdio: 'pipe', cwd: rootDir })
      } catch (e) {
        error('Unable to commit', e)
      }
    })
    substep($style.green('✔ Committed'))
    await wait('Creating tag', async () => {
      try {
        await run('git', [ 'tag', '-a', `v${targetVersion}`, '-m', `v${targetVersion}` ], { stdio: 'pipe', cwd: rootDir })
      } catch (e) {
        error('Unable to create tag', e)
      }
    })
    substep($style.green('✔ Tagged'))
    log() // blank line

    log($style.green(`✔ Release v${targetVersion} successfuly created`))
    log()

    log($style.yellow('IMPORTANT: You should now run ') + 'git push origin --follow-tags')
    log()
  }
}
