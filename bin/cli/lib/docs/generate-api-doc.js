const fs = require('fs-extra')
const jsdoc2md = require('jsdoc-to-markdown')
const path = require('path')
const juisy = require('../../../../src')

// utils
const {
  rootDir,
  $style,
  merge,
  wait,
  log,
  step,
  substep
} = juisy.utils

// Default config
const defaultConfig = {
  outputDir: 'documentation',
  jsdoc: {
    configPath: undefined
  },
  jsdoc2md: {
    partial: []
  }
}

/**
 * Generate API doc function
 * @param {object} [options = {}] - The options object
 * @returns {void}
 */
module.exports = async function generateApiDoc (options = {}) {
  // Process options
  options = merge(defaultConfig, options)

  const docs = {
    API: {
      sourcePath: path.resolve(rootDir, './src/index.js'),
      destPath: path.resolve(rootDir, './documentation/api.md')
    },
    Utils: {
      sourcePath: path.resolve(rootDir, './src/utils/index.js'),
      destPath: path.resolve(rootDir, './documentation/utils.md')
    }
  }

  /**
   * Generate doc
   */
  step('Generate doc')

  // Loop on docs
  for (const name in docs) {
    const sourcePath = docs[name].sourcePath
    const destPath = docs[name].destPath

    let genResult
    await wait(`Generating "${name}"`, async () => {
      genResult = await jsdoc2md.render({
        'no-cache': Boolean(options.jsdoc.configPath),
        files: sourcePath,
        template: `# ${name} documentation\n\n{{>main}}`,
        partial: options.jsdoc2md.partial,
        configure: options.jsdoc.configPath
      })
    })
    // f*cking windows compat:
    // jsdoc2md will generate CR (\r) character for @example blocks content line endings
    genResult = genResult.replace(/\r(\n)?/g, '\n')
    // Write output
    await wait(`Writing file "${path.basename(destPath)}"`, async () => {
      await fs.writeFile(destPath, genResult, { encoding: 'utf8' })
    })
    substep(`File "${path.basename(destPath)}" successfuly generated`)
  }

  substep($style.green('✔ Success'), { last: true })
  log() // Blank line
}
