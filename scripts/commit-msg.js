const juisy = require('../src')

const {
  $style,
  abort,
  log,
  rootDir,
  step,
  substep,
  run
} = juisy.utils

/**
 * commit-msg git hook
 */
;(async function () {
  step('Git hook: commit-msg')

  // Get git commit msg path from args
  const args = process.argv.slice(2)
  const gitMsgPath = args[0]

  try {
    // original is: npx --no -- commitlint --edit ${1}
    // we replace "${1}" by gitMsgPath arg
    await run('npx', [
      '--no',
      '--',
      'commitlint',
      '--edit',
      gitMsgPath
    ], { cwd: rootDir })
  } catch (e) {
    substep($style.red('❌ Git hook: "commit-msg" failed. Please check ./COMMIT_CONVENTION.md for more informations.'), { last: true })
    abort(1) // Abort with error
  }

  // Everything is okay
  substep($style.green('✔ Git hook: "commit-msg" passed'), { last: true })

  log() // blank line
})()
