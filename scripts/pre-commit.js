const juisy = require('../src')

const {
  $style,
  error,
  log,
  rootDir,
  step,
  substep,
  run
} = juisy.utils

/**
 * Pre commit git hook
 */
;(async function () {
  step('Git hook: pre-commit')

  try {
    // npx lint-staged
    await run('npx', [
      'lint-staged'
    ], { stdio: 'pipe', cwd: rootDir })
  } catch (e) {
    error('Git hook: "commit-msg" error: ', e)
  }

  // Everything is okay
  substep($style.green('✔ Git hook: "pre-commit" passed'), { last: true })

  log() // blank line
})()
