/**
 * @hperchec/readme-generator Template EJS data example file
 */

const markdownTable = require('markdown-table') // ! v3 not compatible, use v2

// Based on the package.json file, get some data and informations
const pkg = require('../../package.json')
const packageName = pkg.name
const packageUrl = `https://www.npmjs.com/package/${packageName}`
const dependencies = pkg.dependencies || {}
const devDependencies = pkg.devDependencies || {}
const peerDependencies = pkg.peerDependencies || {}
const author = pkg.author
const contributors = pkg.contributors || []
const license = pkg.license || 'Unknown'
const homepage = pkg.homepage
const projectUrl = pkg.repository.url
const issuesUrl = pkg.bugs.url

// Output a markdown formatted table from a js object
// Like:
// |name|version|
// |----|-------|
// |    |       |
function getMdDependencies (deps) {
  return markdownTable([
    [ 'name', 'version' ],
    ...(Object.entries(deps))
  ])
}

/**
 * Return author link
 * @param {Object} author
 * @return {string}
 */
function getMdAuthor (author) {
  return '[' + author.name + '](' + author.url + ')'
}

/**
 * Return markdown list of persons
 * @param {Array} contributors
 * @return {String}
 */
function getMdContributors (contributors) {
  let mdString = ''
  contributors.forEach((person) => {
    mdString += '- [' + person.name + '](' + person.url + ')\n'
  })
  return mdString
}

/**
 * Export data for readme file templating
 */
module.exports = {
  packageName,
  packageUrl,
  projectUrl,
  homepage,
  issuesUrl,
  dependencies: getMdDependencies(dependencies),
  devDependencies: getMdDependencies(devDependencies),
  peerDependencies: getMdDependencies(peerDependencies),
  author: getMdAuthor(author),
  contributors: getMdContributors(contributors),
  license
}
