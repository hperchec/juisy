const fs = require('fs-extra')
const path = require('path')
const juisy = require('@hperchec/juisy')

const {
  $style,
  abort,
  log,
  rootDir,
  step,
  substep,
  run
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'reset',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Reset git hooks',
  /**
   * Builder
   * @param {object} yargs - yargs
   * @returns {object} Returns yargs
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {object} argv - The argv
   * @returns {void}
   */
  handler: async function (argv) {
    /**
     * Reset git hooks
     */
    step('Reset git hooks')

    // Create temporary empty configuration file
    const tempConfigFilePath = './__TEMP_SIMPLE_GIT_HOOKS_CONFIG__.json' // relative to project root folder
    fs.writeFileSync(path.resolve(rootDir, tempConfigFilePath), '{}')

    // Run command with empty configuration
    let commandError = false
    try {
      await run('npx', [
        'simple-git-hooks',
        tempConfigFilePath
      ], { stdio: 'pipe', cwd: rootDir })
    } catch (e) {
      commandError = e
    }

    // Don't forget to always remove temporary file
    fs.unlinkSync(path.resolve(rootDir, tempConfigFilePath))

    // If error
    if (commandError) {
      substep($style.red('❌ Unable to reset git hooks.'), { last: true })
      abort(1) // Abort with error
    } else {
      // Everything is okay
      substep($style.green('✔ Git hooks successfuly reset'), { last: true })
      log() // blank line
    }
  }
}
