# Awesome project

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[*TOC*]]

## 🚀 Get started

```sh
# Let's go!
```

## 📙 Documentation

Please check the [documentation](https://www.npmjs.com/package/@hperchec/juisy)

## 🧱 Dependencies

<details>
<summary>Global</summary>

<%= dependencies %>

</details>

<details>
<summary>Dev</summary>

<%= devDependencies %>

</details>

<details>
<summary>Peer</summary>

<%= peerDependencies %>

</details>

## ✍ Author

<%= author %>

## 📄 License

<%= license %>

----

Made with ❤ by <%= author %>
