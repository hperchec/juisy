// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'git-hooks <command>',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Git relative commands',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs.commandDir('git_hooks_cmds', { extensions: [ 'cjs' ] })
      .demandCommand(1, ('Command is missing. See help to learn more.').red)
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {}
}
