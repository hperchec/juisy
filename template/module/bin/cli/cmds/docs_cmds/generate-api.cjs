const fs = require('fs-extra')
const path = require('path')
const juisy = require('@hperchec/juisy')

const {
  rootDir,
  $style,
  log,
  step,
  substep,
  error,
  abort
} = juisy.utils

const generateAPIDoc = require('../../lib/docs/generate-api-doc.cjs')

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'generate:api',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate docs from source code',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    yargs.option('c', {
      alias: 'config',
      type: 'string',
      default: './.docs/api/docs.config.cjs',
      describe: 'Path to custom config file relative to root folder (default: "./.docs/api/docs.config.cjs")',
      requiresArg: true
    })
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    let options = {}

    /**
     * Process config option
     */
    step('Configuration')
    const configPath = path.resolve(rootDir, argv.config)
    // Check if file exist
    if (fs.existsSync(configPath)) {
      options = require(configPath)
    } else {
      // Else log error and exit
      error(`Can't find config file: "${configPath}"`)
      abort(1)
    }
    substep($style.green('✔ Successfuly configured'), { last: true })
    log() // blank line

    /**
     * Call generateAPIDoc function
     */
    await generateAPIDoc(options)
  }
}
