#!/usr/bin/env node

'use strict'

const cli = require('./cli.cjs')

cli().parse(process.argv.slice(2))
