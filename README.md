<!--
  ⚠ IMPORTANT
  This file is generated with **@hperchec/readme-generator**. Don't edit it.
-->
# 🍊 juisy

<!-- markdownlint-disable line-length -->
![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

[![release](https://img.shields.io/gitlab/v/release/hperchec/juisy?include_prereleases&logo=gitlab&sort=semver)](https://gitlab.com/hperchec/juisy/-/releases)
[![pipeline-status](https://gitlab.com/hperchec/juisy/badges/main/pipeline.svg)](https://gitlab.com/hperchec/juisy/commits/main)
[![package](https://img.shields.io/npm/v/@hperchec/juisy?logo=npm)](https://www.npmjs.com/package/@hperchec/juisy)
[![downloads](https://img.shields.io/npm/dw/@hperchec/juisy?logo=npm)](https://www.npmjs.com/package/@hperchec/juisy)
[![issues](https://img.shields.io/gitlab/issues/open/hperchec/juisy?gitlab_url=https%3A%2F%2Fgitlab.com)](https://gitlab.com/hperchec/juisy/issues)
![license](https://img.shields.io/gitlab/license/hperchec/juisy?gitlab_url=https%3A%2F%2Fgitlab.com)
<!-- markdownlint-enable line-length -->

**Table of contents**:

<!-- markdownlint-disable-next-line emphasis-style -->
[[_TOC_]]

## What is juisy?


- 🔧 [Embedded CLI tool](https://gitlab.com/hperchec/juisy/-/blob/main/documentation/api.md#juisycli--yargs) that uses yargs under the ground
- 📕 Generate your project documentation and README file with [@hperchec/readme-generator](https://www.npmjs.com/package/@hperchec/readme-generator)
- 🕵️‍♂️ Automatic linting with [lint-staged](https://www.npmjs.com/package/lint-staged) at pre-commit git hook
- 🦾 Respect [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) with [commitlint](https://commitlint.js.org/) at commit-msg git hook

## 🚀 Get started

Install package:

```sh
npm install @hperchec/juisy
```

Squeeze!

```sh
npx juisy squeeze
```

You will get:

```sh
./
├─ bin/ # contains CLI
│  └─ cli/
│     ├─ cmds/ # commands dir
│     ├─ lib/ # libraries used by commands
│     └─ index.js # CLI entry file
├─ .docs/
│  └─ readme # contains configuration for @hperchec/readme-generator
└─ ...
```

...and some new scripts in your `package.json` file:

```js
"docs", // generate documentation
"docs:api", // only api
"docs:readme", // only README file
"release", // make a release
"changelog" // generate CHANGELOG file
"postinstall" // for git hook
```

Just use `--help` option to show help:

```sh
node ./bin/cli --help
```

## 🧱 Dependencies

It will install the following packages as peer dependencies:

| name                               | version  |
| ---------------------------------- | -------- |
| @commitlint/cli                    | ^17.7.2  |
| @commitlint/config-conventional    | ^17.7.0  |
| @github/markdownlint-github        | ^0.6.0   |
| @hperchec/jsdoc-plugin-define      | ^1.0.1   |
| @hperchec/readme-generator         | ^3.0.0   |
| chalk                              | ^4.1.2   |
| conventional-changelog-cli         | ^4.1.0   |
| deepmerge                          | ^4.3.1   |
| eslint                             | ^8.51.0  |
| eslint-config-standard             | ^17.1.0  |
| eslint-plugin-disable              | ^2.0.3   |
| eslint-plugin-import               | ^2.29.1  |
| eslint-plugin-jsdoc                | ^48.0.6  |
| eslint-plugin-n                    | ^16.1.0  |
| eslint-plugin-promise              | ^6.1.1   |
| execa                              | ^5.1.1   |
| fs-extra                           | ^10.1.0  |
| glob                               | ^10.3.10 |
| indent-string                      | ^4.0.0   |
| lint-staged                        | ^14.0.1  |
| markdown-table                     | ^2.0.0   |
| markdownlint-cli2                  | ^0.12.1  |
| markdownlint-cli2-formatter-pretty | ^0.0.6   |
| prompts                            | ^2.4.2   |
| semver                             | ^7.3.7   |
| simple-git-hooks                   | ^2.9.0   |
| strip-ansi                         | ^6.0.0   |
| yargs                              | ^17.7.2  |

## 📙 Documentation

### API

> See [API] documentation](https://gitlab.com/hperchec/juisy/-/blob/main/documentation/api.md)

### Utils

> See [utils documentation](https://gitlab.com/hperchec/juisy/-/blob/main/documentation/utils.md)

## ✍ Author

[Hervé Perchec](https://gitlab.com/herveperchec)

## 📄 License

ISC

----

Made with ❤ by [Hervé Perchec](https://gitlab.com/herveperchec)

----

*README.md - this file was auto generated with [@hperchec/readme-generator](https://www.npmjs.com/package/@hperchec/readme-generator). Don't edit it.*
